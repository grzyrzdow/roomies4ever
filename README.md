# Roomies4Ever

Platform that enable users finding roommates. The platform was made as engineering project with two co-workers. This repository is the cloned version the of original repository.

***Features:***
* Logging with Google account or with standats credentials (email + password)
* Managing account (email, password, phone number, going vip)
* Being Vip makes the user receive sms message when someone books his room
* Sending emails to users when some action related to their room happens
* Creating, updating and deleting room offers
* Booking offers for speific time using calendary (The remaining time will bi available for others ussers to book)
* Adding room to favourites
* Sending message to room owner (Messeges go to owner's email)
* Canceling booking
* Marking room location on a map
* Searching for rooms by location, filtering by price and ordering by prace, name and creation date
* Managing entities with admin panel
* Custom error pages

# Deployed version

Dployed version is available [**here**](https://roomies4ever.herokuapp.com).
