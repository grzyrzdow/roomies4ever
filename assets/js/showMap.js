import * as L from "leaflet";
import $ from "jquery";

var lat = $(".lat").data("lat");
var lng = $(".lng").data("lng");

var mymap = L.map("mapid", { closePopupOnClick: false }).setView(
  [lat, lng],
  12
);

L.tileLayer(
  "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
  {
    maxZoom: 18,
    id: "mapbox.streets"
  }
).addTo(mymap);

var newMarker = new L.marker([lat, lng]).addTo(mymap);
