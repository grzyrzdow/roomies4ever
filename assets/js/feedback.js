import $ from "jquery";
import { notify } from "./toast";

var button = $("#submit");

var title = $("#title");
var message = $("#message");

var textarea = $("textarea");
textarea.on("input", autoResize);

function autoResize() {
  this.style.height = "auto";
  this.style.height = this.scrollHeight + "px";
}

button.click(function() {
  $.ajax({
    type: "POST",
    url: "/feedback/message",
    data: {
      title: title.val(),
      message: message.val()
    },
    dataType: "json",
    success: function(response, textStatus, xhr) {
      title.val("");
      message.val("");
      notify(response.type, response.header, response.message);
    },
    error: function(xhr, textStatus, errorThrown) {
      notify(
        xhr.responseJSON.type,
        xhr.responseJSON.header,
        xhr.responseJSON.message
      );
    }
  });
});
