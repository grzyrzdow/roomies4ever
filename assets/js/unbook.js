import $ from "jquery";
import { notify } from "./toast";

var button = $("a[id^=unbook]");

button.click(function(event) {
  const booking = event.target.getAttribute("data-booking");
  $.ajax({
    type: "DELETE",
    url: "/unbook",
    data: {
      booking: booking
    },
    dataType: "json",
    success: function(response, textStatus, xhr) {
      notify(response.type, response.header, response.message);
      $("tr[id=" + booking + "]").remove();
    },
    error: function(xhr, textStatus, errorThrown) {
      notify(
        xhr.responseJSON.type,
        xhr.responseJSON.header,
        xhr.responseJSON.message
      );
    }
  });
});
