import $ from "jquery";
import { notify } from "./toast";

var email = null;
var phoneNumber = null;
var oldPassword = null;
var newPassword = null;
var newPasswordRep = null;

const submit = $("#submit");
const emailInput = $("#user-email");
const phoneInput = $("#user-phone-number");
const oldPassInput = $("#user-old-pass");
const newPassInput = $("#user-new-pass");
const newPassRepInput = $("#user-new-pass-rep");
const upgradeButton = $("#upgrade-btn");
const upgradeDesc = $(".upgrade-desc");

var initPhone = phoneInput.val();
var initEmail = emailInput.val();

submit.click(function() {
  $.ajax({
    type: "PUT",
    url: "/apply-user-settings",
    data: {
      email: email,
      phoneNumber: phoneNumber,
      oldPassword: oldPassword,
      newPassword: newPassword,
      newPasswordRep: newPasswordRep
    },
    dataType: "json",
    success: function(response, textStatus, xhr) {
      if (email != null) {
        initEmail = email;
      }
      if (phoneNumber != null) {
        initPhone = phoneNumber;
      }
      email = null;
      phoneNumber = null;
      oldPassword = null;
      newPassword = null;
      newPasswordRep = null;
      if (response.message != "false")
        notify(response.type, response.header, response.message);
      submit.removeClass("active");
      oldPassInput.val("");
      newPassInput.val("");
      newPassRepInput.val("");
    },
    error: function(xhr, textStatus, errorThrown) {
      notify(
        xhr.responseJSON.type,
        xhr.responseJSON.header,
        xhr.responseJSON.message
      );
    }
  });
});
emailInput.on("input", function() {
  email = emailInput.val();
  if (email == "" || email == initEmail) email = null;
  checkForChanges();
});
phoneInput.on("input", function() {
  phoneNumber = phoneInput.val();
  if (phoneNumber == "" || phoneNumber == initPhone) phoneNumber = null;
  checkForChanges();
});
oldPassInput.on("input", function() {
  oldPassword = oldPassInput.val();
  if (oldPassword == "") oldPassword = null;
  checkForChanges();
});
newPassInput.on("input", function() {
  newPassword = newPassInput.val();
  oldPassword = oldPassInput.val();
  if (newPassword == "") newPassword = null;
  checkForChanges();
});
newPassRepInput.on("input", function() {
  newPasswordRep = newPassRepInput.val();
  if (newPasswordRep == "") newPasswordRep = null;
  checkForChanges();
});
var checkForChanges = function() {
  if (
    email != null ||
    phoneNumber != null ||
    (oldPassword != null && newPassword != null && newPasswordRep != null)
  ) {
    submit.addClass("active");
  } else {
    submit.removeClass("active");
  }
};

upgradeButton.click(function() {
  $.ajax({
    type: "PUT",
    url: "/vip",
    data: {},
    dataType: "json",
    success: function(response, textStatus, xhr) {
      if (response.message != "false")
        notify(response.type, response.header, response.message);
      upgradeButton.remove();
      upgradeDesc.text(
        "Jesteś użytkownikiem premium do dnia " +
          response.until +
          ", Dziękujemy!"
      );
    },
    error: function(xhr, textStatus, errorThrown) {
      notify(
        xhr.responseJSON.type,
        xhr.responseJSON.header,
        xhr.responseJSON.message
      );
    }
  });
});
