import $ from "jquery";
import { notify } from "./toast";

var n = $(".notification");
var message = n.data("message");
var header = n.data("header");
var type = n.data("type");

function get(name) {
  if (
    (name = new RegExp("[?&]" + encodeURIComponent(name) + "=([^&]*)").exec(
      location.search
    ))
  )
    return decodeURIComponent(name[1]);
}

window.onload = function() {
  if (get("n") == "999" && message) {
    notify("", header, message, true);
  } else if (message) {
    notify(type, header, message);
  }
};
