import * as L from "leaflet";
import $ from "jquery";

var mymap = L.map("mapid").setView([52.409538, 16.931992], 13);

L.tileLayer(
  "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
  {
    maxZoom: 18,
    id: "mapbox.streets"
  }
).addTo(mymap);

var newMarker = new L.marker([52.409538, 16.931992]).addTo(mymap);

function onMapClick(e) {
  newMarker.setLatLng(e.latlng);
}

mymap.on("click", onMapClick);

var id = document.querySelector(".id").dataset.id;

var submit = document.getElementById("submit");
submit.onclick = function() {
  $.ajax({
    type: "POST",
    url: "/save/coordinates",
    data: {
      id: id,
      location1: newMarker.getLatLng().lat,
      location2: newMarker.getLatLng().lng
    },
    dataType: "json",
    success: function(response) {
      window.location.href = response.redirect;
    }
  });
};
