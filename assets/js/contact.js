import $ from "jquery";
import { notify } from "./toast";

var button = $("#send-message");

var user = button.data("user");
var room = button.data("room");

button.click(function() {
  $.ajax({
    type: "POST",
    url: "/contact",
    data: {
      userId: user,
      roomId: room,
      message: $("#message").val()
    },
    dataType: "json",
    success: function(response, textStatus, xhr) {
      notify(response.type, response.header, response.message);
      $("#message").val("");
      $("#contactForm").css("display", "none");
    },
    error: function(xhr, textStatus, errorThrown) {
      notify(
        xhr.responseJSON.type,
        xhr.responseJSON.header,
        xhr.responseJSON.message
      );
    }
  });
});
