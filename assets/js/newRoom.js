import $ from "jquery";
import { notify } from "./toast";

var textarea = document.querySelector(".desc");
textarea.addEventListener("input", autoResize, false);

var currentImg = 2;
const newImg = $("#new");

newImg.click(function() {
  $("#form_image" + currentImg).removeClass("inactive");
  currentImg++;
  if (currentImg > 5) {
    newImg.remove();
  }
  newImg.addClass("disabled");
});

function autoResize() {
  this.style.height = "auto";
  this.style.height = this.scrollHeight + "px";
}

$('input[type="file"]').change(function(e) {
  var fileName = e.target.files[0].name;
  if (e.target.id == "form_image" + (currentImg - 1)) {
    newImg.removeClass("disabled");
  }
  $("label[for=" + e.target.id + "]").text(fileName);
});
