import React, { Component } from "react";
import { toast, Slide, Zoom, Flip, Bounce } from "react-toastify";

//notification defaults
var configuration = {
  closeOnClick: false,
  transition: Flip
};
function navigteToTest() {
  window.location.href = "/test/show";
}

function notify(
  type = "success",
  header = "Sukces",
  message = "",
  navigate = false
) {
  if (navigate) {
    configuration.onClick = navigteToTest;
    configuration.autoClose = 10000;
  }
  toast.configure(configuration);
  if (type == "success") {
    toast.success(
      <div>
        <center>
          <b>{header}</b>
          <div>{message}</div>
        </center>
      </div>
    );
  } else if (type == "warn") {
    toast.warn(
      <div>
        <center>
          <b>{header}</b>
        </center>
        <div>{message}</div>
      </div>
    );
  } else if (type == "error") {
    toast.error(
      <div>
        <center>
          <b>{header}</b>
        </center>
        <div>{message}</div>
      </div>
    );
  } else if (type == "info") {
    toast.info(
      <div>
        <center>
          <b>{header}</b>
        </center>
        <div>{message}</div>
      </div>
    );
  } else {
    toast(
      <div>
        <center>
          <b>{header}</b>
        </center>
        <div>{message}</div>
      </div>
    );
  }
}

export { notify };
