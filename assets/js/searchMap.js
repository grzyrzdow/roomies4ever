import React, { Component } from "react";
import Calendar from "react-calendar";
import reactDom from "react-dom";
import * as L from "leaflet";
import $ from "jquery";
import { notify } from "./toast";

const scrollUp = $(".scroll-up-container");
const range = $("#radius");
const minPrice = $("#priceMin");
const maxPrice = $("#priceMax");
const notification = $("#notification");
const type = notification.data("type");
const header = notification.data("header");
const message = notification.data("message");
const found = notification.data("found");
const submit = $("#submit");
var sortV = $("select");
const today = new Date();
const tomorrow = new Date(today);
tomorrow.setDate(tomorrow.getDate() + 7);
var dates = {
  dateF: today,
  dateT: tomorrow
};
if (found) {
  if ($("body").width() <= 720) {
    window.scrollTo(0, 1000);
  } else {
    window.scrollTo(0, 550);
  }
}

window.addEventListener("load", function() {
  if (getUrlParameter("radius") > 0) {
    newCircle.setRadius(getUrlParameter("radius"));
    $("#radius").val(getUrlParameter("radius") / 40);
  }
  if (
    getUrlParameter("lat") >= -90 &&
    getUrlParameter("lat") <= 90 &&
    getUrlParameter("lng") >= -180 &&
    getUrlParameter("lng") <= 180
  ) {
    newCircle.setLatLng([getUrlParameter("lat"), getUrlParameter("lng")]);
    mymap.setView([getUrlParameter("lat"), getUrlParameter("lng")]);
  }
  if (getUrlParameter("df") != 0 && getUrlParameter("df") != "1000-01-01") {
    dates.dateF = new Date(getUrlParameter("df"));
  }
  if (getUrlParameter("dt") != 0 && getUrlParameter("dt") != "9999-01-01") {
    dates.dateT = new Date(getUrlParameter("dt"));
  }
  if (getUrlParameter("priceMin") > 0) {
    $("#priceMin").val(getUrlParameter("priceMin"));
  }
  if (
    getUrlParameter("priceMax") > 0 &&
    getUrlParameter("priceMax") != "Infinity"
  ) {
    $("#priceMax").val(getUrlParameter("priceMax"));
  }
  if (getUrlParameter("orderBy") != "0") {
    sortV.val(getUrlParameter("orderBy"));
    $(".filter-option-inner-inner").text(
      $("option[value=" + getUrlParameter("orderBy") + "]").text()
    );
  }
  reactDom.render(
    <SearchCalendar />,
    document.getElementById("search-calendar")
  );
  if (type) {
    notify(type, header, message);
  }
});
$(window).scroll(function() {
  if ($(window).scrollTop() >= 500) {
    if (!scrollUp.hasClass("active")) {
      scrollUp.addClass("active");
    }
  } else {
    if (scrollUp.hasClass("active")) {
      scrollUp.removeClass("active");
    }
  }
  if ($(window).scrollTop() >= 750) {
    submit.css("visibility", "hidden");
  } else {
    submit.css("visibility", "visible");
  }
});
scrollUp.click(function() {
  window.scrollTo(0, 100);
});

range.keyup(function() {
  range.val(range.val().replace(new RegExp("[^0-9]*"), ""));
});
minPrice.keyup(function() {
  minPrice.val(minPrice.val().replace(new RegExp("[^0-9]*"), ""));
});
maxPrice.keyup(function() {
  maxPrice.val(maxPrice.val().replace(new RegExp("[^0-9]*"), ""));
});

class SearchCalendar extends Component {
  state = {
    date: [dates.dateF, dates.dateT],
    minDetail: "month",
    selectRange: true,
    returnValue: "range",
    minDate: today
  };
  onChange = date => {
    dates.dateF = date[0];
    dates.dateT = date[1];
  };

  render() {
    return (
      <div>
        <Calendar
          onChange={this.onChange}
          minDetail={this.state.minDetail}
          selectRange={this.state.selectRange}
          returnValue={this.state.returnValue}
          value={this.state.date}
          minDate={this.state.minDate}
        />
      </div>
    );
  }
}
var mymap = L.map("mapid", { closePopupOnClick: false }).setView(
  [52.409538, 16.931992],
  13
);

L.tileLayer(
  "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
  {
    maxZoom: 18,
    id: "mapbox.streets"
  }
).addTo(mymap);
var newCircle = new L.Circle([52.409538, 16.931992], {
  color: "#2780E3",
  fillColor: "#2780E3",
  radius: 1000
}).addTo(mymap);

$("#radius").on("input", function() {
  newCircle.setRadius($(this).val() * 40);
});

function onMapClick(e) {
  newCircle.setLatLng(e.latlng);
}

mymap.on("click", onMapClick);
$("#submit").on("click", function() {
  var priceMin = document.getElementById("priceMin").value;
  var priceMax = document.getElementById("priceMax").value;
  var dateFrom = dates.dateF;
  var dateTo = dates.dateT;
  var orderBy = sortV.val();
  dateFrom =
    dateFrom.getFullYear() +
    "-" +
    (dateFrom.getMonth() + 1) +
    "-" +
    dateFrom.getDate();
  dateTo =
    dateTo.getFullYear() +
    "-" +
    (dateTo.getMonth() + 1) +
    "-" +
    dateTo.getDate();

  if (!priceMin) priceMin = 0;
  if (!priceMax) priceMax = Infinity;

  if (Number(priceMin) > Number(priceMax)) {
    notify(
      "error",
      "Błąd",
      "Cena minimalna powinna być mniejsza niż cena maksymalna"
    );
  } else {
    window.location.href =
      "/search?lat=" +
      newCircle.getLatLng().lat +
      "&lng=" +
      newCircle.getLatLng().lng +
      "&radius=" +
      newCircle.getRadius() +
      "&df=" +
      dateFrom +
      "&dt=" +
      dateTo +
      "&priceMin=" +
      priceMin +
      "&priceMax=" +
      priceMax +
      "&orderBy=" +
      orderBy +
      "&first=false";
  }
});

var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split("&"),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split("=");

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined
        ? true
        : decodeURIComponent(sParameterName[1]);
    }
  }
};
