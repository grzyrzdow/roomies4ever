import $ from "jquery";
import { notify } from "./toast";

var button = $("#add-favourite");

var user = button.data("user");
var room = button.data("room");
var heart = $("#add-favourite i");

button.click(function() {
  $.ajax({
    type: "POST",
    url: "/add-favourite",
    data: {
      userId: user,
      roomId: room
    },
    dataType: "json",
    success: function(response, textStatus, xhr) {
      if (heart.text() == "favorite") {
        heart.text("favorite_border");
      } else {
        heart.text("favorite");
      }
      notify(response.type, response.header, response.message);
    },
    error: function(xhr, textStatus, errorThrown) {
      notify(
        xhr.responseJSON.type,
        xhr.responseJSON.header,
        xhr.responseJSON.message
      );
    }
  });
});
