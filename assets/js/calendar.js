import React, { Component } from "react";
import Calendar from "react-calendar";
import reactDom from "react-dom";
import $ from "jquery";
import { notify } from "./toast";

const info = $("#availability-calendar");
const bookButton = $("#book-confirm");
const dateFrom = info.data("dateFrom");
const dateTo = info.data("dateTo");
const tools = $(".room-tools");
const title = $(".title");
var room = bookButton.data("room");
var dates = {
  dateF: 0,
  dateT: 0
};

var bookingsString = bookButton.data("bookings");
var bookings = new Array();
for (var i = 0; i < bookingsString.length; i++) {
  bookings.push(JSON.parse(bookingsString[i]));
}
const notifyInfo = $("#notification");

class AvailabilityCaledar extends Component {
  state = {
    minDetail: "year",
    selectRange: true,
    returnValue: "range",
    minDate: new Date(Math.max(new Date(), new Date(dateFrom))),
    maxDate: new Date(dateTo),
    activeStartDate: new Date(Math.max(new Date(), new Date(dateFrom))),
    tileDisabled: ({ activeStartDate, date, view }) => {
      var total = false;
      for (var i = 0; i < bookingsString.length; i++) {
        var df = new Date(bookings[i].dateFrom);
        var dt = new Date(bookings[i].dateTo);
        total = total || (date >= df && date <= dt);
      }
      return total;
    }
  };

  render() {
    return (
      <div>
        <Calendar
          value={this.state.date}
          minDetail={this.state.minDetail}
          selectRange={this.state.selectRange}
          returnValue={this.state.returnValue}
          onClickDay={this.xd}
          minDate={this.state.minDate}
          maxDate={this.state.maxDate}
          tileDisabled={this.state.tileDisabled}
          activeStartDate={this.state.activeStartDate}
        />
      </div>
    );
  }
}

class BookCalendar extends Component {
  state = {
    date: [new Date("2019-12-06"), new Date("2019-12-07")],
    minDetail: "year",
    selectRange: true,
    returnValue: "range",
    minDate: new Date(Math.max(new Date(), new Date(dateFrom))),
    maxDate: new Date(dateTo),
    activeStartDate: new Date(Math.max(new Date(), new Date(dateFrom))),
    tileDisabled: ({ activeStartDate, date, view }) => {
      var total = false;
      for (var i = 0; i < bookingsString.length; i++) {
        var df = new Date(bookings[i].dateFrom);
        var dt = new Date(bookings[i].dateTo);
        total = total || (date >= df && date <= dt);
      }
      return total;
    }
  };
  onChange = date => {
    dates.dateF = date[0];
    dates.dateT = date[1];
  };

  render() {
    return (
      <div>
        <Calendar
          onChange={this.onChange}
          minDetail={this.state.minDetail}
          selectRange={this.state.selectRange}
          returnValue={this.state.returnValue}
          onClickDay={this.xd}
          minDate={this.state.minDate}
          maxDate={this.state.maxDate}
          tileDisabled={this.state.tileDisabled}
          activeStartDate={this.state.activeStartDate}
        />
      </div>
    );
  }
}

reactDom.render(
  <AvailabilityCaledar />,
  document.getElementById("availability-calendar")
);
reactDom.render(<BookCalendar />, document.getElementById("book-calendar"));

const book = function() {
  if (dates.dateF == 0 && dates.dateT == 0) {
    notify(
      "error",
      "Błąd",
      "Proszę wybrać termin zaznaczając datę rozpoczęcia i końca rezerwacji"
    );
  }
  $.ajax({
    type: "POST",
    url: "/book",
    data: {
      roomId: room,
      dateFrom: [
        dates.dateF.getFullYear(),
        dates.dateF.getMonth(),
        dates.dateF.getDate()
      ],
      dateTo: [
        dates.dateT.getFullYear(),
        dates.dateT.getMonth(),
        dates.dateT.getDate()
      ]
    },
    dataType: "json",
    success: function(response, textStatus, xhr) {
      var url = window.location.href;
      if (url.indexOf("?") > 1) {
        url += "&code=" + response.code;
      } else {
        url += "?code=" + response.code;
      }
      url += "&df=" + response.df + "&dt=" + response.dt;
      window.location.href = url;
    },
    error: function(xhr, textStatus, errorThrown) {
      var url = window.location.href;
      if (url.indexOf("?") > 1) {
        url += "&code=" + xhr.responseJSON.code;
      } else {
        url += "?code=" + xhr.responseJSON.code;
      }
      window.location.href = url;
    }
  });
};

bookButton.click(function() {
  book();
});

window.addEventListener("load", function() {
  var type = notifyInfo.data("type");
  var header = notifyInfo.data("header");
  var message = notifyInfo.data("message");
  if (type) {
    notify(type, header, message);
  }
  window.history.replaceState({}, document.title, window.location.pathname);
});

$(window).scroll(function() {
  if ($(window).scrollTop() >= 100) {
    title.css("margin-left", "10%");
    tools.css("margin-right", "10%");
  } else {
    title.css("margin-left", "0");
    tools.css("margin-right", "30px");
  }
});
