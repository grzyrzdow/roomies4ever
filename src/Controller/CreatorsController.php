<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CreatorsController extends AbstractController
{
    /**
     * @Route("/creators", name="app_creators")
     */
    public function index(Request $request)
    {
        return $this->render('creators/index.html.twig');
    }
}
