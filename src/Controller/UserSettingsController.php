<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\Users;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class UserSettingsController extends AbstractController
{
    /**
     * @Route("/user/settings", name="app_user_settings")
     */
    public function rederTemplate()
    {
        return $this->render('user_settings/index.html.twig', [
            'controller_name' => 'UserSettingsController',
        ]);
    }

    /**
     * @Route("/apply-user-settings", name="app_apply_user_settings")
     */
    public function ApplyUserSettings(Request $request, UserPasswordEncoderInterface $passwordEncoder): ?JsonResponse
    {
        $response = new JsonResponse();

        $data = $request->request->all();

        $user = $this->getUser();

        $userRepository = $this->getDoctrine()->getRepository(Users::class);
        $entityManager = $this->getDoctrine()->getManager();

        $email = $data["email"];
        $phoneNumber = $data["phoneNumber"];
        $oldPassword = $data["oldPassword"];
        $newPassword = $data["newPassword"];
        $newPasswordRep = $data["newPasswordRep"];

        if (!filter_var($email, FILTER_VALIDATE_EMAIL) && $email != null) {
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Podaj właściwy adres email'
            ]);
            return $response;
        } elseif (count($userRepository->findBy(['email' => $email])) != 0 && $email != $user->getEmail()) {
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Adres email zajęty'
            ]);
            return $response;
        }
        if (!preg_match("/^[0-9]{9}$/", $phoneNumber) && $phoneNumber != null) {
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Podaj numer telefonu jako dziewięć cyfr'
            ]);
            return $response;
        }
        if ($user->getGoogleId() != null && $oldPassword != null) {
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Będąc zalogowanym przez Google nie logujesz się z pomocą hasła'
            ]);
            return $response;
        }
        if (!$passwordEncoder->isPasswordValid($user, $oldPassword) && $oldPassword != null) {
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Hasło nieprawidłowe'
            ]);
            return $response;
        }
        if ($newPassword != $newPasswordRep && $newPassword != null) {
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Nowe hasła muszą być identyczne'
            ]);
            return $response;
        }
        if (strlen($newPassword) < 8 && $newPassword != null) {
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Nowe hasło musi zawierać co najmniej 8 znaków'
            ]);
            return $response;
        }
        if ($email != null) {
            $user->setEmail($email);
        }
        if ($phoneNumber != null) {
            $user->setPhonenumber($phoneNumber);
        }
        if ($newPassword != null) {
            $user->setPassword($passwordEncoder->encodePassword($user, $newPassword));
        }
        if (($email != null || $phoneNumber != null || $newPassword != null)) {
            $entityManager->flush($user);
            $response->setStatusCode(JsonResponse::HTTP_OK);
            $response->setData([
                "type" => "default",
                'header' => 'Sukces',
                'message' => 'Zapisano zmiany'
            ]);
            return $response;
        } else {
            $response->setStatusCode(JsonResponse::HTTP_OK);
            $response->setData([
                "type" => "default",
                'header' => 'Sukces',
                'message' => 'false'
            ]);
            return $response;
        }
    }
}
