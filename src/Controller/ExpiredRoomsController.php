<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


use App\Entity\Rooms;
use App\Entity\Booking;

class ExpiredRoomsController extends AbstractController
{
    /**
     * @Route("/expire-rooms", name="expired_rooms")
     */
    public function index(): ?JsonResponse
    {
        $response = new JsonResponse();
        $rooms = $this->getDoctrine()->getRepository(Rooms::class)->findAll();
        $bookings = $this->getDoctrine()->getRepository(Booking::class)->findAll();
        $entityManager = $this->getDoctrine()->getManager();
        $roomsCounter = 0;
        $bookingsDeleted = 0;

        foreach ($rooms as $room) {
            if ($room->getDateTo() < new \DateTime() && $room->getIsDeleted() == false) {
                $room->setIsDeleted(true);
                $roomsCounter++;
            }
        }
        foreach ($bookings as $booking) {
            if ($booking->getDateTo() < new \DateTime()) {
                $entityManager->remove($booking);
                $bookingsDeleted++;
            }
        }
        $entityManager->flush();

        $response->setStatusCode(200);
        $response->setData([[
            'rooms-deleted' => $roomsCounter,
            'bookings-deleted' => $bookingsDeleted,
        ]]);
        return $response;
    }
}
