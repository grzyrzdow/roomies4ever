<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Knp\Component\Pager\PaginatorInterface;

use App\Entity\Users;
use App\Entity\Rooms;
use App\Entity\Favourite;

class FavouriteController extends AbstractController
{
    /**
     * @Route("/add-favourite", name="app_favourite")
     */
    public function addFavourite(Request $request): ?JsonResponse
    {
        $data = $request->request->all();

        $user = $this->getDoctrine()->getRepository(Users::class)->find($this->getUser()->getId());
        $room = $this->getDoctrine()->getRepository(Rooms::class)->find($data["roomId"]);
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Favourite::class);
        $res = new JsonResponse();

        $fav = $repository->findBy([
            'user' => $user,
            'room' => $room
        ]);

        if (count($fav) === 0) {
            $favourite = new Favourite();
            $favourite->setRoom($room);
            $favourite->setUser($user);

            $entityManager->persist($favourite);
            $entityManager->flush();

            $res->setData([
                "type" => "default",
                'header' => 'Sukces!',
                'message' => 'Dodano do ulubionych'
            ]);
            return $res;
        } else {
            $entityManager->remove($fav[0]);
            $entityManager->flush();

            $res->setData([
                "type" => "default",
                'header' => 'Sukces!',
                'message' => 'Usunięto z ulubionych'
            ]);
            return $res;
        }
    }

    /**
     * @Route("/favourites", name="app_show_favourites")
     */
    public function showFavourites(Request $request, PaginatorInterface $paginator)
    {
        $repository = $this->getDoctrine()->getRepository(Favourite::class);
        $favourites = $repository->findBy([
            'user' => $this->getUser()->getId(),
        ]);
        $rooms = [];
        foreach ($favourites as $favourite) {
            $room = $this->getDoctrine()->getRepository(Rooms::class)->find($favourite->getRoom());
            if (!$room->getIsDeleted()) {
                array_push($rooms, $room);
            }
        }
        return $this->render('favourites/show.html.twig', [
            'pagination' => $paginator->paginate(
                $rooms,
                $request->query->getInt('page', 1),
                30
            )
        ]);
    }
}
