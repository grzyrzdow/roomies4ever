<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Smsapi\Client\SmsapiHttpClient;
use Smsapi\Client\Feature\Sms\Bag\SendSmsBag;

use App\Entity\Users;
use App\Entity\Rooms;

class SmsController extends AbstractController
{
    /**
     * @Route("/sms", name="app_sms")
     */
    public function index(Request $request): ?JsonResponse
    {
        $res = new JsonResponse();
        $sms = SendSmsBag::withMessage('601196207', 'Pisiek, wejdz na messengera bo telefon mi padł xd');
        $service = (new SmsapiHttpClient())
            ->smsapiPlService($_SERVER['SMS_API_TOKEN']);
        $service->smsFeature()
            ->sendSms($sms);
        return $res;
    }
}
