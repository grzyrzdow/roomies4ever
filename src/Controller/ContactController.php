<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


use App\Entity\Users;
use App\Entity\Rooms;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $request): ?JsonResponse
    {
        $data = $request->request->all();

        $user = $this->getDoctrine()->getRepository(Users::class)->find($data["userId"]);
        $room = $this->getDoctrine()->getRepository(Rooms::class)->find($data["roomId"]);
        $message = $data['message'];
        $res = new JsonResponse();

        if (strlen($message) == 0) {
            $res->setStatusCode(400);
            $res->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Wiadomość nie może być pusta'
            ]);
            return $res;
        } elseif (strlen($message) > 250) {
            $res->setStatusCode(400);
            $res->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Wiadomość nie może być dłuższa niż 250 znaków'
            ]);
            return $res;
        }
        if ($this->getUser()->getEmail() == $user->getEmail()) {
            $res->setStatusCode(400);
            $res->setData([
                "type" => "default",
                'header' => 'Gapa!',
                'message' => 'Radzimy nie wysyłać wiadomości do siebie'
            ]);
            return $res;
        }

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($this->getUser()->getEmail(), "Roomies4Ever");
        $email->setSubject("Wiadomość od " . $user->getFname() . " " . $user->getSname() . " dotycząca " . $room->getTitle());


        $email->addTo($user->getEmail());
        $email->addContent(
            "text/plain",
            $message
        );

        $sendgrid = new \SendGrid($_SERVER['SENDGRID_API_KEY']);
        try {
            $response = $sendgrid->send($email);
            $res->setStatusCode(200);
            $res->setData([
                "type" => "default",
                'header' => 'Sukces!',
                'message' => 'Wiadomość wysłana!'
            ]);
        } catch (Exception $e) {
            $res->setStatusCode(500);
            $res->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Wystąpił problem, prosimy o próbę ponownego wysłania wiadomości'
            ]);
        }
        return $res;
    }
}
