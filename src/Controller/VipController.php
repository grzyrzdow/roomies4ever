<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Users;

class VipController extends AbstractController
{
    /**
     * @Route("/vip", name="app_go_vip")
     */
    public function goVip(): ?JsonResponse
    {
        $response = new JsonResponse();
        $manager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if ($user->getIsVip()) {
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Jesteś już użytkownikiem premium'
            ]);
        } elseif ($user->getPhonenumber() == null) {
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Wymagane jest podanie numeru telefonu'
            ]);
        } else {
            $user->setIsVip(true);
            $date = new \DateTime();
            $user->setVipUntil($date->add(date_interval_create_from_date_string("30 days")));
            $manager->flush();
            $response->setStatusCode(JsonResponse::HTTP_OK);
            $response->setData([
                "until" => $date->format("d.m.Y"),
                "type" => "default",
                'header' => 'Gratulacje',
                'message' => 'Jesteś użytkownikiem premium'
            ]);
        }
        return $response;
    }

    /**
     * @Route("/unvip", name="app_unvip")
     */
    public function Unvip(): ?JsonResponse
    {
        $response = new JsonResponse();
        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();
        $manager = $this->getDoctrine()->getManager();
        $i = 0;

        foreach ($users as $user) {
            if ($user->getIsVip() && $user->getVipUntil() < new \DateTime()) {
                $user->setIsVip(false);
                $i++;
            }
        }
        $manager->flush();
        $response->setData([
            'data' => [
                'vipsExpired' => $i
            ]
        ]);
        return $response;
    }
}
