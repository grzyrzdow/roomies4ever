<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Entity\Rooms;
use App\Entity\Booking;
use Knp\Component\Pager\PaginatorInterface;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="app_search")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $data = $request->query->all();
        $first = $data['first'];
        $radius = $data['radius'];
        $lat = $data['lat'];
        $lng = $data['lng'];
        $dateFrom = $data['df'];
        if ($dateFrom == '1000-01-01' || $dateFrom == 0) {
            $dateFrom = '9999-01-01';
        }
        $dateTo = $data['dt'];
        if ($dateTo == '9999-01-01' || $dateTo == 0) {
            $dateTo = '1000-01-01';
        }
        $priceMin = $data['priceMin'];
        if (!is_numeric($priceMin)) {
            $priceMin = PHP_INT_MAX;
        }
        $priceMax = $data['priceMax'];
        if ($priceMax == "Infinity" || !is_numeric($priceMax)) {
            $priceMax = PHP_INT_MAX;
        }
        $order = $data['orderBy'];
        $orderBy = "uploaded DESC";
        if ($order == 0) {
            $orderBy = "uploaded DESC";
        } elseif ($order == 1) {
            $orderBy = "uploaded ASC";
        } elseif ($order == 2) {
            $orderBy = "price ASC";
        } elseif ($order == 3) {
            $orderBy = "price DESC";
        }
        $R = 6371;
        $rad = $radius / 1000;
        $maxLat = $lat + rad2deg($rad / $R);
        $minLat = $lat - rad2deg($rad / $R);
        $maxLng = $lng + rad2deg(asin($rad / $R) / cos(deg2rad($lat)));
        $minLng = $lng - rad2deg(asin($rad / $R) / cos(deg2rad($lat)));

        $entityManager = $this->getDoctrine()->getManager();

        $query = $entityManager->createQuery(
            'SELECT r
            FROM App\Entity\Rooms r
            WHERE r.location1 BETWEEN :minLat AND :maxLat
            AND r.location2  BETWEEN :minLng AND :maxLng
            AND r.price BETWEEN :priceMin AND :priceMax
            AND r.dateFrom <= :dateFrom
            AND r.dateTo >= :dateTo
            AND r.isDeleted != 1
            AND r.isFinished = 1
            AND r.isBooked = 0
            ORDER BY r.' . $orderBy
        )->setParameters([
            'minLat' => $minLat,
            'maxLat' => $maxLat,
            'minLng' => $minLng,
            'maxLng' => $maxLng,
            'priceMin' => $priceMin,
            'priceMax' => $priceMax,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo
        ]);

        $rooms = $query->execute();

        $bookings = $this->getDoctrine()->getRepository(Booking::class)->findAll();

        $toDelete = [];
        foreach ($bookings as $book) {
            if (((new \DateTime($dateFrom) >= $book->getDateFrom() && new \DateTime($dateFrom) <= $book->getDateTo()) || (new \DateTime($dateTo) >= $book->getDateFrom() && new \DateTime($dateTo) <= $book->getDateTo())) || (new \DateTime($dateFrom) < $book->getDateFrom() && new \DateTime($dateTo) > $book->getDateTo())) {
                array_push($toDelete, $book->getRoom());
            }
        }
        array_unique($toDelete);

        foreach ($rooms as $i => $room) {
            foreach ($toDelete as $d) {
                if ($room == $d) {
                    unset($rooms[$i]);
                    break;
                }
            }
        }
        if ($first == "true") {
            return $this->render('search/index.html.twig', [
                'pagination' => $paginator->paginate(
                    $rooms,
                    $request->query->getInt('page', 1),
                    20
                ),
                'found' => "false",
                'type' => "false",
                'header' => 'Sukces',
                'message' => ''
            ]);
        } elseif (count($rooms) == 0) {
            return $this->render('search/index.html.twig', [
                'pagination' => $paginator->paginate(
                    $rooms,
                    $request->query->getInt('page', 1),
                    20
                ),
                'found' => "false",
                'type' => "info",
                'header' => 'Brak wyników',
                'message' => 'Nie znaleziono pokoi dla podanych kryteriów'
            ]);
        } else {
            return $this->render('search/index.html.twig', [
                'pagination' => $paginator->paginate(
                    $rooms,
                    $request->query->getInt('page', 1),
                    20
                ),
                'found' => "true",
                'type' => "false",
                'header' => 'Sukces',
                'message' => ''
            ]);
        }
    }
}
