<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\Optionquestions;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class TestController extends Controller
{
    /**
     * @Route("/test/show", name="app_test_show")
     * Method({"GET", "POST"})
     */
    public function showTest(Request $request, UrlGeneratorInterface $urlGenerator): Response
    {
        $count = count($this->getDoctrine()
            ->getRepository(Optionquestions::class)
            ->findAll());

        $this->denyAccessUnlessGranted('view', $this->getUser(), "Już wykonałeś test");
        $questions2 = $this->getQuestions(2, 11);
        $questions4 = $this->getQuestions(4, 2);
        $questions5 = $this->getQuestions(5, 2);
        $questions6 = $this->getQuestions(6, 1);
        $questionsImage = $this->getQuestions(9, 1, true);

        $maxPoints = $this->getMaxWeights($questions2) + $this->getMaxWeights($questions4) + $this->getMaxWeights($questions5) + $this->getMaxWeights($questions6) + $this->getMaxWeights($questionsImage);

        $form = $this->createFormBuilder();
        for ($i = 0; $i < 11; $i++) {
            $form->add('question' . $i, ChoiceType::class, array(
                'label' => $questions2[$i]->getText(),
                'choices' => array(
                    $questions2[$i]->getOption1() => $questions2[$i]->getWeight1(),
                    $questions2[$i]->getOption2() => $questions2[$i]->getWeight2(),
                ),
                'expanded' => true,
                'multiple' => false,
            ));
        }

        for ($i = 11; $i < 13; $i++) {
            $form->add('question' . $i, ChoiceType::class, array(
                'label' => $questions4[$i - 11]->getText(),
                'choices' => array(
                    $questions4[$i - 11]->getOption1() => $questions4[$i - 11]->getWeight1(),
                    $questions4[$i - 11]->getOption2() => $questions4[$i - 11]->getWeight2(),
                    $questions4[$i - 11]->getOption3() => $questions4[$i - 11]->getWeight3(),
                    $questions4[$i - 11]->getOption4() => $questions4[$i - 11]->getWeight4(),
                ),
                'expanded' => true,
                'multiple' => false,
            ));
        }

        for ($i = 13; $i < 15; $i++) {
            $form->add('question' . $i, ChoiceType::class, array(
                'label' => $questions5[$i - 13]->getText(),
                'choices' => array(
                    $questions5[$i - 13]->getOption1() => $questions5[$i - 13]->getWeight1(),
                    $questions5[$i - 13]->getOption2() => $questions5[$i - 13]->getWeight2(),
                    $questions5[$i - 13]->getOption3() => $questions5[$i - 13]->getWeight3(),
                    $questions5[$i - 13]->getOption4() => $questions5[$i - 13]->getWeight4(),
                    $questions5[$i - 13]->getOption5() => $questions5[$i - 13]->getWeight5(),
                ),
                'expanded' => true,
                'multiple' => false,
            ));
        }

        for ($i = 15; $i < 16; $i++) {
            $form->add('question' . $i, ChoiceType::class, array(
                'label' => $questions6[$i - 15]->getText(),
                'choices' => array(
                    $questions6[$i - 15]->getOption1() => $questions6[$i - 15]->getWeight1(),
                    $questions6[$i - 15]->getOption2() => $questions6[$i - 15]->getWeight2(),
                    $questions6[$i - 15]->getOption3() => $questions6[$i - 15]->getWeight3(),
                    $questions6[$i - 15]->getOption4() => $questions6[$i - 15]->getWeight4(),
                    $questions6[$i - 15]->getOption5() => $questions6[$i - 15]->getWeight5(),
                    $questions6[$i - 15]->getOption6() => $questions6[$i - 15]->getWeight6(),
                ),
                'expanded' => true,
                'multiple' => false,
            ));
        }
        $images = array();
        for ($i = 16; $i < 17; $i++) {
            $form->add('question' . $i, ChoiceType::class, array(
                'label' => $questionsImage[$i - 16]->getText(),
                'choices' => array(
                    "1" => $questionsImage[$i - 16]->getWeight1(),
                    "2" => $questionsImage[$i - 16]->getWeight2(),
                    "3" => $questionsImage[$i - 16]->getWeight3(),
                    "4" => $questionsImage[$i - 16]->getWeight4(),
                    "5" => $questionsImage[$i - 16]->getWeight5(),
                    "6" => $questionsImage[$i - 16]->getWeight6(),
                    "7" => $questionsImage[$i - 16]->getWeight7(),
                    "8" => $questionsImage[$i - 16]->getWeight8(),
                    "9" => $questionsImage[$i - 16]->getWeight9(),
                ),
                'attr' => ['class' => 'css-checkbox'],
                'expanded' => true,
                'multiple' => false,
            ));
            array_push($images, $questionsImage[$i - 16]->getOption1());
            array_push($images, $questionsImage[$i - 16]->getOption2());
            array_push($images, $questionsImage[$i - 16]->getOption3());
            array_push($images, $questionsImage[$i - 16]->getOption4());
            array_push($images, $questionsImage[$i - 16]->getOption5());
            array_push($images, $questionsImage[$i - 16]->getOption6());
            array_push($images, $questionsImage[$i - 16]->getOption7());
            array_push($images, $questionsImage[$i - 16]->getOption8());
            array_push($images, $questionsImage[$i - 16]->getOption9());
        }

        $form->add('submit', SubmitType::class, [
            'label' => 'Zapisz',
            'attr' => ['class' => 'btn btn-primary btn-lg'],
        ]);

        $form = $form->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $points = 0;
            for ($i = 0; $i < 17; $i++) {
                $points += $data['question' . $i];
            }
            $percentage = $points / $maxPoints;
            $p9 = 1 - abs(0.58783 - $percentage);
            $p8 = 1 - abs(0.82094 - $percentage);
            $p7 = 1 - abs(0.19932 - $percentage);
            $p6 = 1 - abs(0.27702 - $percentage);
            $p5 = 1 - abs(0.07432 - $percentage);
            $p4 = 1 - abs(0.34121 - $percentage);
            $p3 = 1 - abs(0.41554 - $percentage);
            $p2 = 1 - abs(0.46959 - $percentage);
            $p1 = 1 - abs(0.51013 - $percentage);
            $user = $this->getUser();
            $entityManager = $this->getDoctrine()->getManager();
            $user->setPersonality1($p1);
            $user->setPersonality2($p2);
            $user->setPersonality3($p3);
            $user->setPersonality4($p4);
            $user->setPersonality5($p5);
            $user->setPersonality6($p6);
            $user->setPersonality7($p7);
            $user->setPersonality8($p8);
            $user->setPersonality9($p9);
            $user->setDidTest(true);
            $entityManager->persist($user);
            $entityManager->flush();

            return new RedirectResponse($urlGenerator->generate('default', [
                'n' => 3
            ]));
        }

        return $this->render('test/show.html.twig', array('form' => $form->createView(), 'images' => $images));
    }


    private function getMaxWeights($questions)
    {
        $sum = 0;
        foreach ($questions as $q) {
            $sum += max($q->getWeight1(), $q->getWeight2(), $q->getWeight3(), $q->getWeight4(), $q->getWeight5(), $q->getWeight6(), $q->getWeight7(), $q->getWeight8(), $q->getWeight9());
        }
        return $sum;
    }

    private function getQuestions($optionCount, $amount, $isImage = false)
    {
        $repo = $this->getDoctrine()->getRepository(Optionquestions::class);
        $questions = [];
        if ($optionCount < 9)
            $query = $repo->CreateQueryBuilder('q')
                ->andWhere('q.option' . ($optionCount + 1) . ' IS NULL')
                ->andWhere('q.option' . $optionCount . ' IS NOT NULL')
                ->andWhere('q.isImage = :isImage')
                ->setParameter('isImage', $isImage)
                ->getQuery();
        else
            $query = $repo->CreateQueryBuilder('q')
                ->andWhere('q.option' . $optionCount . ' IS NOT NULL')
                ->andWhere('q.isImage = :isImage')
                ->setParameter('isImage', $isImage)
                ->getQuery();

        $q = $query->execute();

        for ($i = 0; $i < $amount; $i++) {
            array_push($questions, $q[$i]);
        }
        return $questions;
    }
}
