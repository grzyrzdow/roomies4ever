<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Debug\Exception\FlattenException;

class ExceptionController extends Controller
{
    public function showException(FlattenException $exception)
    {
        if ($exception->getStatusCode() == 403)
            return $this->render('error/error403.html.twig', [
                'statusCode' => $exception->getStatusCode(),
                'message' => $exception->getMessage()
            ]);
        else if ($exception->getStatusCode() == 404)
            return $this->render('error/error404.html.twig', [
                'statusCode' => $exception->getStatusCode(),
                'message' => $exception->getMessage()
            ]);
        else
            return $this->render('error/error.html.twig', [
                'statusCode' => $exception->getStatusCode(),
                'message' => $exception->getMessage()
            ]);
    }
}
