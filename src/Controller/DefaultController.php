<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(Request $request)
    {
        $message = "Strona główna";
        if ($request->query->get('n') == 3) {
            return $this->render('default/index.html.twig', [
                "type" => "default",
                'header' => 'Dziękujemy',
                'message' => 'Dziękujemy za wypełnienie testu! Od tej pory, jeśli inny użytkownik także wypełnił test, będziesz widział, jak bardzo do siebie pasujecie!'
            ]);
        } else if ($request->query->get('n') == 2) {
            return $this->render('default/index.html.twig', [
                "type" => "default",
                'header' => 'Dziękujemy',
                'message' => 'Dodano ofertę pomyślnie'
            ]);
        } else if ($request->query->get('n') == 999) {
            return $this->render('default/index.html.twig', [
                "type" => "default",
                'header' => 'Test osobowości',
                'message' => 'Zauważyliśmy, że nie wypełniłeś jeszcze testu osobowości. Rozwiąż test i ciesz się lepszym dopasowaniem przyszłego współlokatora! Kliknij w powiaomienie aby przejść do testu'
            ]);
        } else
            return $this->render('default/index.html.twig', [
                "type" => "success",
                'header' => 'Dziękujemy',
                'message' => 'false'
            ]);
    }
}
