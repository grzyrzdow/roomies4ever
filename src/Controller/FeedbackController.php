<?php

namespace App\Controller;

use Doctrine\Migrations\Configuration\Exception\JsonNotValid;
use ProxyManager\Factory\RemoteObject\Adapter\JsonRpc;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class FeedbackController extends AbstractController
{
    /**
     * @Route("/feedback", name="app_feedback")
     */
    public function index()
    {
        return $this->render('feedback/index.html.twig', [
            'controller_name' => 'FeedbackController',
        ]);
    }

    /**
     * @Route("/feedback/message", name="app_feedback_message")
     */
    public function feedbackMessage(Request $request): ?JsonResponse
    {
        $response = new JsonResponse();
        $data = $request->request->all();

        $title = $data["title"];
        $message = $data["message"];

        if (strlen($message) == 0 || strlen($title) == 0) {
            $response->setStatusCode(400);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Treść zgłoszenia i tytuł nie mogą być puste'
            ]);
            return $response;
        } elseif (strlen($message) > 1000) {
            $response->setStatusCode(400);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Treść zgłoszenia może zawierać do 1000 znaków'
            ]);
            return $response;
        } elseif (strlen($title) > 30) {
            $response->setStatusCode(400);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Tytuł może zawierać do 30 znaków'
            ]);
            return $response;
        }

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("bug.report@r4e.com", "Roomies4Ever- Zgłoszenie błędu");
        $email->setSubject($title);
        $email->addTo("roomies4ever.contact@gmail.com");
        $email->addContent(
            "text/plain",
            $message
        );
        $sendgrid = new \SendGrid($_SERVER['SENDGRID_API_KEY']);
        try {
            $res = $sendgrid->send($email);
            $response->setStatusCode(200);
            $response->setData([
                "type" => "default",
                'header' => 'Sukces!',
                'message' => 'Dziękujemy za zgłoszenie!'
            ]);
        } catch (Exception $e) {
            $response->setStatusCode(500);
            $response->setData([
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Wystąpił problem, prosimy o próbę ponownego wysłania zgłoszenia'
            ]);
            return $response;
        }

        return $response;
    }
}
