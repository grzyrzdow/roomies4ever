<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Knp\Component\Pager\PaginatorInterface;
use Smsapi\Client\SmsapiHttpClient;
use Smsapi\Client\Feature\Sms\Bag\SendSmsBag;

use App\Entity\Rooms;
use App\Entity\Booking;


class BookController extends AbstractController
{
    /**
     * @Route("/book", name="app_book")
     */
    public function index(Request $request): ?JsonResponse
    {
        $response = new JsonResponse();
        $entityManager = $this->getDoctrine()->getManager();

        $data = $request->request->all();
        $user = $this->getUser();
        $room = $this->getDoctrine()->getRepository(Rooms::class)->find($data["roomId"]);
        $currentBookigs = $this->getDoctrine()->getRepository(Booking::class)->findBy([
            'room' => $room
        ]);
        $dateFrom = $data['dateFrom'];
        $dateTo = $data['dateTo'];
        $dateFrom = new \DateTime($dateFrom[0] . '-' . ((int) $dateFrom[1] + 1) . '-' . $dateFrom[2]);
        $dateTo = new \DateTime($dateTo[0] . '-' . ((int) $dateTo[1] + 1) . '-' . $dateTo[2]);

        if ($room->getUserId() == $this->getUser()) {
            $response->setStatusCode(JsonResponse::HTTP_METHOD_NOT_ALLOWED);
            $response->setData([
                "code" => 3
            ]);
            return $response;
        }

        foreach ($currentBookigs as $b) {
            if (($dateFrom >= $b->getDateFrom() && $dateFrom <= $b->getDateTo()) || ($dateTo >= $b->getDateFrom() && $dateTo <= $b->getDateTo())) {
                $response->setStatusCode(JsonResponse::HTTP_METHOD_NOT_ALLOWED);
                $response->setData([
                    "code" => 2
                ]);
                return $response;
            } elseif ($dateFrom < $b->getDateFrom() && $dateTo > $b->getDateTo()) {
                $response->setStatusCode(JsonResponse::HTTP_METHOD_NOT_ALLOWED);
                $response->setData([
                    "code" => 1,

                ]);
                return $response;
            }
        }

        $booking = new Booking();
        $booking->setUser($user);
        $booking->setRoom($room);
        $booking->setDateFrom($dateFrom);
        $booking->setDateTo($dateTo);

        $entityManager->persist($booking);
        $entityManager->flush();

        $owner = $room->getUserId();
        $message = "Twój pokój, o nazwie " . $room->getTitle() . " został zarezerwowany w terminie " . $dateFrom->format('d.m.Y') . " - " . $dateTo->format('d.m.Y') . ".\nPozdrawiamy, Roomies4Ever.";

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("roomies4ever.contact@gmail.com", "Roomies4Ever");
        $email->setSubject("Rezerwacja pokoju " . $room->getTitle());
        $email->addTo($owner->getEmail());
        $email->addContent(
            "text/plain",
            $message
        );
        $sendgrid = new \SendGrid($_SERVER['SENDGRID_API_KEY']);
        $emailResponse = $sendgrid->send($email);

        if ($owner->getIsVip()) {
            $sms = SendSmsBag::withMessage($owner->getPhonenumber(), $message);
            $sms->encoding = 'utf-8';
            $service = (new SmsapiHttpClient())
                ->smsapiPlService($_SERVER['SMS_API_TOKEN']);
            $service->smsFeature()
                ->sendSms($sms);
        }

        $response->setStatusCode(200);
        $response->setData([
            "code" => 999,
            'df' => $dateFrom->format('d.m.Y'),
            'dt' => $dateTo->format('d.m.Y')
        ]);
        return $response;
    }

    /**
     * @Route("/unbook", name="app_unbook", methods={"DELETE"})
     * 
     */
    public function unbook(Request $request): ?JsonResponse
    {
        $response = new JsonResponse();
        $data = $request->request->all();
        $booking = $this->getDoctrine()->getRepository(Booking::class)->find($data["booking"]);
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($booking);
        $entityManager->flush();

        $room = $booking->getRoom();
        $owner = $room->getUserId();
        $message = "Rezerwacja Twojego pokoju, o nazwie " . $room->getTitle() . " została odwołana w terminie " . $booking->getDateFrom()->format('d.m.Y') . " - " . $booking->getDateTo()->format('d.m.Y') . ".\nPozdrawiamy, Roomies4Ever.";

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("roomies4ever.contact@gmail.com", "Roomies4Ever");
        $email->setSubject("Odwołana rezerwacja pokoju " . $room->getTitle());
        $email->addTo($owner->getEmail());
        $email->addContent(
            "text/plain",
            $message
        );
        $sendgrid = new \SendGrid($_SERVER['SENDGRID_API_KEY']);
        $emailResponse = $sendgrid->send($email);

        if ($owner->getIsVip()) {
            $sms = SendSmsBag::withMessage($owner->getPhonenumber(), $message);
            $sms->encoding = 'utf-8';
            $service = (new SmsapiHttpClient())
                ->smsapiPlService($_SERVER['SMS_API_TOKEN']);
            $service->smsFeature()
                ->sendSms($sms);
        }

        $response->setData([
            "type" => "default",
            'header' => 'Sukces',
            'message' => 'Odwołano rezerwację'
        ]);
        return $response;
    }

    /**
     * @Route("/booked", name="app_show_booked")
     */
    public function showBooked(Request $request, PaginatorInterface $paginator)
    {
        $repository = $this->getDoctrine()->getRepository(Booking::class);
        $bookings = $repository->findBy([
            'user' => $this->getUser()
        ]);
        return $this->render('booked/show.html.twig', [
            'pagination' => $paginator->paginate(
                $bookings,
                $request->query->getInt('page', 1),
                10
            )
        ]);
    }
}
