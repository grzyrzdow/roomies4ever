<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Users;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends Controller
{
    /**
     * @Route("/message/to={id}", name="app_message")
     */
    public function index(Request $request, $id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Users::class);
        $user = $repository->find($id);

        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('title', TextType::class)
            ->add('message', TextAreaType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { }



        return $this->render('message/send.html.twig', [
            'form' => $form->CreateView(),
        ]);
    }
}
