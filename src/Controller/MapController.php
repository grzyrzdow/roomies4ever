<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Rooms;


class MapController extends Controller
{
    /**
     * @Route("/room/location/{id}", name="app_map")
     */
    public function index(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository(Rooms::class);
        $room = $repository->find($id);
        $this->denyAccessUnlessGranted('locate', $room, "Nie możesz zmienić lokacji w tej ofercie");
        return $this->render('room/getLocation.html.twig');
    }


    /**
     * @Route("/save/coordinates", name="app_save_coordinates")
     * Method({"POST"})
     */
    public function getCoordinates(Request $request)
    {
        $data = $request->request->all();

        $repository = $this->getDoctrine()->getRepository(Rooms::class);
        $room = $repository->find($data['id']);

        $entityManager = $this->getDoctrine()->getManager();

        $room->setLocation1((float)$data['location1']);
        $room->setLocation2((float)$data['location2']);
        $room->setIsFinished(true);

        $entityManager->persist($room);
        $entityManager->flush();

        return new JsonResponse([
            'redirect' => '/?n=2'
        ]);
    }
}
