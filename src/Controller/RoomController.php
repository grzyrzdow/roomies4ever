<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Entity\Rooms;
use App\Entity\Users;
use App\Entity\Booking;
use App\Entity\Favourite;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Service\BondCounter;

class RoomController extends Controller
{
    /**
     * @Route("/room/new", name="app_new_room")
     * Method({"POST"})
     */
    public function getRoom(Request $request, UrlGeneratorInterface $urlGenerator): Response
    {
        $room = new Rooms();
        $form = $this->createFormBuilder($room);
        $form->add('title', TextType::class, [
            'label' => 'Nazwa oferty',
            'attr' => [
                'placeholder' => 'Nazwa oferty',
                'class' => 'form-control info'
            ]
        ])
            ->add('area', NumberType::class, [
                'invalid_message' => 'Jako metraż, podaj liczbę zakresu od 1 do 1000.',
                'label' => 'Metraż pokoju',
                'attr' => [
                    'placeholder' => 'Metraż',
                    'class' => 'form-control info'
                ]
            ])
            ->add('price', NumberType::class, [
                'label' => 'Cena w zł/noc',
                'invalid_message' => 'Jako cenę, podaj liczbę.',
                'attr' => [
                    'placeholder' => 'Cena w zł/noc',
                    'class' => 'form-control info'
                ]
            ])
            ->add('dateFrom', DateType::class, [
                'label' => 'Dostępny od',
                'attr' => [
                    'class' => 'date'
                ]
            ])
            ->add('dateTo', DateType::class, [
                'label' => 'Dostępny do',
                'attr' => [
                    'class' => 'date'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Opis',
                'invalid_message' => 'Data końcowa jest niepoprawna',
                'attr' => [
                    'placeholder' => 'Tutaj wpisz opis...',
                    'class' => 'form-control desc',
                    'rows' => '9'
                ]
            ])
            ->add('image1', FileType::class, [
                'required' => false,
                'label' => 'Dodaj zdjęcie',
                'attr' => [
                    'class' => 'file',
                    "id" => "1"
                ]
            ])
            ->add('image2', FileType::class, [
                'required' => false,
                'label' => 'Dodaj zdjęcie',
                'attr' => [
                    'class' => 'file inactive'
                ]
            ])
            ->add('image3', FileType::class, [
                'required' => false,
                'label' => 'Dodaj zdjęcie',
                'attr' => [
                    'class' => 'file inactive'
                ]
            ])
            ->add('image4', FileType::class, [
                'required' => false,
                'label' => 'Dodaj zdjęcie',
                'attr' => [
                    'class' => 'file inactive'
                ]
            ])
            ->add('image5', FileType::class, [
                'required' => false,
                'label' => 'Dodaj zdjęcie',
                'attr' => [
                    'class' => 'file inactive'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Dalej',
                'attr' => [
                    'class' => 'btn btn-primary btn-lg submit',
                    'onClick' => 'ajaxRequest()',
                ]
            ]);

        $form = $form->getForm();
        $form->handleRequest($request);
        $info = array('Image2', 'Image3', 'Image4', 'Image5');
        $max = sizeof($info);
        $iterator = 2;


        if ($form->isSubmitted() && $form->isValid()) {

            $uploaded1 = \Cloudinary\Uploader::upload($room->getImage1(), [
                "folder" => "RoomsImages/"
            ]);
            for ($i = 0; $i < $max; $i++) {
                if ($room->{"get" . $info[$i]}() && isset($room)) {
                    ${"uploaded" . $iterator} = \Cloudinary\Uploader::upload($room->{"get" . $info[$i]}(), [
                        "folder" => "RoomsImages/"
                    ]);
                    if (preg_match('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', ${"uploaded" . $iterator}['secure_url'])) {
                        $room->{"setImage" . $iterator}(${"uploaded" . $iterator}['secure_url']);
                        $iterator++;
                    }
                }
            }

            // set null for not uploaded images
            for ($i = $iterator; $i < $max + 2; $i++) {
                $room->{"setImage" . $i}(NULL);
            }

            $entityManager = $this->getDoctrine()->getManager();

            $room->setImage1($uploaded1['secure_url']);
            $room->setUserid($this->getUser());

            $room->setUploaded(new \DateTime());

            $entityManager->persist($room);
            $entityManager->flush();

            return new RedirectResponse($urlGenerator->generate('app_map', ['id' => $room->getId()]));
        }

        return $this->render(
            'room/getRoom.html.twig',
            [
                'form' => $form->CreateView(),
            ]
        );
    }

    /**
     * @Route("/room/show/{id}", name="app_show_room")
     * @ParamConverter("rooms", class="App\Entity\Rooms")
     */
    public function show($id, BondCounter $bondCounter, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Rooms::class);
        $room = $repository->find($id);
        $bookings = $this->getDoctrine()->getRepository(Booking::class)->findBy([
            'room' => $room
        ]);
        $bookingsJSON = [];

        foreach ($bookings as $b) {
            array_push($bookingsJSON, $b->toJSON());
        }

        $this->denyAccessUnlessGranted('view', $room, "Prawdopodobnie oferta została zarchiwizowana");

        $repository = $this->getDoctrine()->getRepository(Users::class);
        $user = $repository->find($room->getUserId()->getId());
        $fav = $this->getDoctrine()->getRepository(Favourite::class)->findBy(
            [
                'room' => $room,
                'user' => $this->getUser()
            ]
        );
        $inFav = count($fav) > 0;
        $owner = [
            'id' => $user->getId(),
            'name' => $user->getFname(),
            'surname' => $user->getSname(),
            'email' => $user->getEmail(),
            'phonenumber' => $user->getPhonenumber()
        ];

        $showBond = false;

        if ($user->getDidTest() && $this->getUser()->getDidTest()) {
            $owner['bond'] = $bondCounter->getBond($this->getUser(), $user);
            $showBond = true;
        }


        if ($request->query->get('code') == 999) {
            return $this->render('room/showRoom.html.twig', [
                'room' => $room,
                'owner' => $owner,
                'showBond' => $showBond,
                'bookings' => $bookingsJSON,
                "type" => "default",
                'header' => 'Sukces',
                'message' => 'Zarezerwowano w terminie ' . $request->query->get('df') . " - " . $request->query->get('dt')
            ]);
        } elseif ($request->query->get('code') == 1) {
            return $this->render('room/showRoom.html.twig', [
                'room' => $room,
                'owner' => $owner,
                'showBond' => $showBond,
                'bookings' => $bookingsJSON,
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Proszę wybrać termin bez przerw'
            ]);
        } elseif ($request->query->get('code') == 2) {
            return $this->render('room/showRoom.html.twig', [
                'room' => $room,
                'owner' => $owner,
                'showBond' => $showBond,
                'bookings' => $bookingsJSON,
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Termin już zajęty'
            ]);
        } elseif ($request->query->get('code') == 3) {
            return $this->render('room/showRoom.html.twig', [
                'room' => $room,
                'owner' => $owner,
                'showBond' => $showBond,
                'bookings' => $bookingsJSON,
                "type" => "error",
                'header' => 'Błąd',
                'message' => 'Nie możesz zarezerwować swojego pokoju'
            ]);
        } else {
            return $this->render('room/showRoom.html.twig', [
                'room' => $room,
                'owner' => $owner,
                'showBond' => $showBond,
                'bookings' => $bookingsJSON,
                "type" => "false",
                'header' => 'Sukces',
                'message' => '',
                'inFav' => $inFav
            ]);
        }
    }

    /**
     * @Route("/room/delete/{id}", name="app_delete_room")
     */
    public function delete($id, Request $request, UrlGeneratorInterface $urlGenerator)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Rooms::class);
        $room = $repository->find($id);

        $this->denyAccessUnlessGranted('edit', $room, "Nie możesz usunąć nie swojej oferty");

        $room->setIsDeleted(true);

        $entityManager->persist($room);
        $entityManager->flush();
        if ($request->query->get('n') == 1)
            return $this->redirect("/userRooms/?n=1");
        else
            return $this->redirect("/userRooms");
    }

    /**
     * @Route("/room/edit/{id}", name="app_edit_room")
     */
    public function edit($id, Request $request, UrlGeneratorInterface $urlGenerator)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Rooms::class);
        $room = $repository->find($id);

        $this->denyAccessUnlessGranted('edit', $room, "Nie możesz edytować nie swojej oferty");

        $form = $this->createFormBuilder($room, [
            'validation_groups' => 'edit'
        ]);
        $form->add('title', TextType::class, [
            'label' => 'Nazwa oferty',
            'attr' => [
                'placeholder' => 'Nazwa oferty',
                'class' => 'form-control info'
            ]
        ])
            ->add('area', NumberType::class, [
                'invalid_message' => 'Jako metraż, podaj liczbę zakresu od 1 do 1000.',
                'label' => 'Metraż pokoju',
                'attr' => [
                    'placeholder' => 'Nazwa oferty',
                    'class' => 'form-control info'
                ]
            ])
            ->add('price', NumberType::class, [
                'label' => 'Cena w zł/noc',
                'invalid_message' => 'Jako cenę, podaj liczbę.',
                'attr' => [
                    'placeholder' => 'Nazwa oferty',
                    'class' => 'form-control info'
                ]
            ])
            ->add('dateFrom', DateType::class, [
                'label' => 'Dostępny od',
                'attr' => [
                    'class' => 'date'
                ]
            ])
            ->add('dateTo', DateType::class, [
                'label' => 'Dostępny do',
                'attr' => [
                    'class' => 'date'
                ]
            ])
            ->add('description', CKEditorType::class, [
                'label' => 'Opis',
                'attr' => [
                    'placeholder' => 'Tutaj wpisz opis...',
                    'class' => 'form-control desc'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz',
                'attr' => [
                    'class' => 'btn btn-primary btn-lg submit'
                ]
            ]);

        $form = $form->getForm();
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $room->setUserid($this->getUser());

            $room->setUploaded(new \DateTime());

            $entityManager->persist($room);
            $entityManager->flush();

            return new RedirectResponse($urlGenerator->generate('app_user_rooms', ['n' => 2]));
        }

        return $this->render(
            'room/editRoom.html.twig',
            [
                'form' => $form->CreateView(),
            ]
        );
    }
}
