<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


use App\Entity\Users;
use App\Entity\Roles;
use App\Entity\Rooms;
use App\Entity\Booking;

class UserController extends Controller
{
    /**
     * @Route("/register", name="app_register")
     * Method({"GET", "POST"})
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, AuthorizationCheckerInterface $authChecker, UrlGeneratorInterface $urlGenerator)
    {
        if (true === $authChecker->isGranted('ROLE_USER')) {
            return $this->redirect('/');
        }
        $user = new Users();

        $form = $this->createFormBuilder($user)
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'label' => 'Email',
                    'placeholder' => 'Email',
                ],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_name' => 'password',
                'second_name' => 'repeatPassword',
                'first_options'  => [
                    'attr' => [
                        'placeholder' => 'Hasło',
                        'class' => 'form-control'
                    ],
                    'label' => 'Hasło'
                ],
                'second_options' => [
                    'attr' => [
                        'placeholder' => 'Powtórz hasło',
                        'class' => 'form-control'
                    ],
                    'label' => 'Powtórz hasło'
                ],
                'invalid_message' => 'Hasła muszą być idntyczne'
            ])
            ->add('fname', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Imię',
                ],
                'label' => 'Imię'
            ])
            ->add('sname', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nazwisko',
                ],
                'label' => 'Nazwisko'
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-lg btn-primary',
                ],
                'label' => 'Zarejestruj się'
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $role = $this->getDoctrine()
                ->getRepository(Roles::class)
                ->findOneBy(['name' => 'user']);

            $user->setPassword($passwordEncoder->encodePassword($user, $form->get('password')->getData()));
            $user->setRoleid($role);
            $entityManager->persist($user);

            $entityManager->flush();

            return new RedirectResponse($urlGenerator->generate('app_login', [
                'n' => 1
            ]));
        }
        return $this->render('security/register.html.twig', array('registrationForm' => $form->createView()));
    }

    /**
     * @Route("/userRooms", name="app_user_rooms")
     * Method({"GET", "POST"})
     */
    public function showUserRooms(Request $request, PaginatorInterface $paginator)
    {
        $repository = $this->getDoctrine()->getRepository(Rooms::class);
        $bRepository = $this->getDoctrine()->getRepository(Booking::class);

        $userRooms = $repository->findBy(
            [
                'userid' => $this->getUser()->getId(),
                'isDeleted' => false,
                'isFinished' => true
            ],
            [
                'uploaded' => 'DESC'
            ]
        );

        $bookings = [];
        foreach ($userRooms as $room) {
            $b = $bRepository->findBy([
                'room' => $room
            ]);
            array_push($bookings, $b);
        }

        if ($request->query->get('n') == 1)
            return $this->render('room/showUserRooms.html.twig', [
                'pagination' => $paginator->paginate(
                    $userRooms,
                    $request->query->getInt('page', 1),
                    10
                ),
                'type' => 'default',
                'header' => 'Sukces',
                'message' => 'Usunięto ofertę',
                'bookings' => $bookings
            ]);
        else if ($request->query->get('n') == 2)
            return $this->render('room/showUserRooms.html.twig', [
                'pagination' => $paginator->paginate(
                    $userRooms,
                    $request->query->getInt('page', 1),
                    10
                ),
                'type' => 'default',
                'header' => 'Sukces',
                'message' => 'Oferta zaktualizowana',
                'bookings' => $bookings
            ]);
        else
            return $this->render('room/showUserRooms.html.twig', [
                'pagination' => $paginator->paginate(
                    $userRooms,
                    $request->query->getInt('page', 1),
                    10
                ),
                'type' => '',
                'header' => '',
                'message' => 'false',
                'bookings' => $bookings
            ]);
    }
}
