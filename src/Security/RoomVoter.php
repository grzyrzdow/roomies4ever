<?php

namespace App\Security;

use App\Entity\Rooms;
use App\Entity\Users;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class RoomVoter extends Voter
{
    const EDIT = 'edit';
    const VIEW = 'view';
    const LOCATE = 'locate';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::EDIT, self::VIEW, self::LOCATE])) {
            return false;
        }

        if (!$subject instanceof Rooms) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof Users) {
            return false;
        }

        /** @var Rooms $room */
        $room = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($room, $user);
            case self::VIEW:
                return $this->canView($room, $user);
            case self::LOCATE:
                return $this->canLocate($room, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canEdit(Rooms $room, Users $user)
    {
        return $user === $room->getUserid() && !$room->getIsDeleted() && $room->getIsFinished();
    }

    private function canView(Rooms $room, Users $user)
    {
        return !$room->getIsDeleted() && $room->getIsFinished() && (!$room->getIsBooked() || $user === $room->getReserving() || $user === $room->getUserid());
    }

    private function canLocate(Rooms $room, Users $user)
    {
        return $user === $room->getUserid() && !$room->getIsDeleted() && !$room->getIsFinished();
    }
}
