<?php

namespace App\Service;

use App\Entity\Users;

class BondCounter
{
    public function getBond(Users $user1, Users $user2)
    {
        $user1Data = [
            $user1->getPersonality1(),
            $user1->getPersonality2(),
            $user1->getPersonality3(),
            $user1->getPersonality4(),
            $user1->getPersonality5(),
            $user1->getPersonality6(),
            $user1->getPersonality7(),
            $user1->getPersonality8(),
            $user1->getPersonality9()
        ];
        $user2Data = [
            $user2->getPersonality1(),
            $user2->getPersonality2(),
            $user2->getPersonality3(),
            $user2->getPersonality4(),
            $user2->getPersonality5(),
            $user2->getPersonality6(),
            $user2->getPersonality7(),
            $user2->getPersonality8(),
            $user2->getPersonality9()
        ];
        $sum = 0;

        for ($i = 0; $i < count($user1Data); $i++) {
            $sum += abs($user1Data[$i] - $user2Data[$i]);
        }
        $bond = 1 - $sum / count($user1Data);
        return floor($bond * 100);
    }
}
