<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rooms
 *
 * @ORM\Table(name="rooms", indexes={@ORM\Index(name="fk_7ca11a96f132696e", columns={"userid"})})
 * @ORM\Entity
 */
class Rooms
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\Length(
     *  min = 1,
     *  max = 20,
     *  minMessage = "Tytuł musi mieć od 1 do 20 znaków",
     *  maxMessage = "Tytuł musi mieć od 1 do 20 znaków"
     * )
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var int
     * @Assert\Range(
     *  min = 1,
     *  max = 1000,
     *  minMessage = "Podaj liczbę z zkresu od 1 do 1000",
     *  maxMessage = "Podaj liczbę z zkresu od 1 do 1000"
     * )
     *
     * @ORM\Column(name="area", type="integer", nullable=false)
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="image1", type="string", length=255, nullable=false)
     * 
     * @Assert\NotBlank(
     *      message = "Przynajmniej jedno zdjęcie jest wymagane"
     * )
     * 
     * @Assert\File(
     *     mimeTypes = {"image/*"},
     *     mimeTypesMessage = "Proszę zamieścić plik będący obrazem.",
     *     maxSize = "2M",
     *     maxSizeMessage = "Plik jest zbyt duży, maksymalny rozmiar to 2MB.",
     * )
     */
    private $image1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image2", type="string", length=255, nullable=true)
     * 
     * @Assert\File(
     *     mimeTypes = {"image/*"},
     *     mimeTypesMessage = "Proszę zamieścić plik będący obrazem.",
     *     maxSize = "2M",
     *     maxSizeMessage = "Plik jest zbyt duży, maksymalny rozmiar to 2MB.",
     * )
     */
    private $image2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image3", type="string", length=255, nullable=true)
     * 
     * @Assert\File(
     *     mimeTypes = {"image/*"},
     *     mimeTypesMessage = "Proszę zamieścić plik będący obrazem.",
     *     maxSize = "2M",
     *     maxSizeMessage = "Plik jest zbyt duży, maksymalny rozmiar to 2MB.",
     * )
     */
    private $image3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image4", type="string", length=255, nullable=true)
     * 
     * @Assert\File(
     *     mimeTypes = {"image/*"},
     *     mimeTypesMessage = "Proszę zamieścić plik będący obrazem.",
     *     maxSize = "2M",
     *     maxSizeMessage = "Plik jest zbyt duży, maksymalny rozmiar to 2MB.",
     * )
     */
    private $image4;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image5", type="string", length=255, nullable=true)
     * 
     * @Assert\File(
     *     mimeTypes = {"image/*"},
     *     mimeTypesMessage = "Proszę zamieścić plik będący obrazem.",
     *     maxSize = "2M",
     *     maxSizeMessage = "Plik jest zbyt duży, maksymalny rozmiar to 2MB.",
     * )
     */
    private $image5;

    /**
     * @var float|null
     *
     * @ORM\Column(name="location1", type="float", precision=10, scale=0, nullable=true)
     */
    private $location1;

    /**
     * @var float|null
     *
     * @ORM\Column(name="location2", type="float", precision=10, scale=0, nullable=true)
     */
    private $location2;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFrom", type="date", nullable=false)
     */
    private $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateTo", type="date", nullable=false)
     */
    private $dateTo;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=16777215, nullable=false)
     * 
     * @Assert\NotBlank(
     * message = "Wpisz opis",
     * groups = {"create", "edit"}
     * )
     */
    private $description;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userid", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $userid;

    /**
     * @var bool
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=false, options= {"default": 0})
     */
    private $isDeleted = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isFinished", type="boolean", nullable=false, options= {"default": 0})
     */
    private $isFinished = false;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="uploaded", type="datetime", nullable=false)
     */
    private $uploaded;

    /**
     * @var bool
     *
     * @ORM\Column(name="isBooked", type="boolean", nullable=false, options= {"default": 0})
     */
    private $isBooked = false;




    public function __construct()
    {
        $this->dateTo = new \DateTime('tomorrow');
        $this->dateFrom = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArea(): ?int
    {
        return $this->area;
    }

    public function setArea(int $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getImage1(): ?string
    {
        return $this->image1;
    }

    public function setImage1(string $image1): self
    {
        $this->image1 = $image1;

        return $this;
    }

    public function getImage2(): ?string
    {
        return $this->image2;
    }

    public function setImage2(?string $image2): self
    {
        $this->image2 = $image2;

        return $this;
    }

    public function getImage3(): ?string
    {
        return $this->image3;
    }

    public function setImage3(?string $image3): self
    {
        $this->image3 = $image3;

        return $this;
    }

    public function getImage4(): ?string
    {
        return $this->image4;
    }

    public function setImage4(?string $image4): self
    {
        $this->image4 = $image4;

        return $this;
    }

    public function getImage5(): ?string
    {
        return $this->image5;
    }

    public function setImage5(?string $image5): self
    {
        $this->image5 = $image5;

        return $this;
    }

    public function getLocation1(): ?float
    {
        return $this->location1;
    }

    public function setLocation1(?float $location1): self
    {
        $this->location1 = $location1;

        return $this;
    }

    public function getLocation2(): ?float
    {
        return $this->location2;
    }

    public function setLocation2(?float $location2): self
    {
        $this->location2 = $location2;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getUserid()
    {
        return $this->userid;
    }

    public function setUserid(?Users $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice(float $price)
    {
        $this->price = $price;

        return $this;
    }

    public function getDateFrom()
    {
        return $this->dateFrom;
    }


    public function setDateFrom(\DateTime $dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    public function getDateTo()
    {
        return $this->dateTo;
    }

    public function setDateTo(\DateTime $dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getUploaded()
    {
        return $this->uploaded;
    }

    public function setUploaded($uploaded)
    {
        $this->uploaded = $uploaded;

        return $this;
    }

    public function getIsFinished()
    {
        return $this->isFinished;
    }

    public function setIsFinished(bool $isFinished)
    {
        $this->isFinished = $isFinished;

        return $this;
    }


    public function getIsBooked()
    {
        return $this->isBooked;
    }


    public function setIsBooked(bool $isBooked)
    {
        $this->isBooked = $isBooked;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }
}
