<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Optionquestions
 *
 * @ORM\Table(name="optionquestions")
 * @ORM\Entity
 */
class Optionquestions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=120, nullable=false)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="option1", type="string", length=180, nullable=false)
     */
    private $option1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="option2", type="string", length=180, nullable=true)
     */
    private $option2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="option3", type="string", length=180, nullable=true)
     */
    private $option3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="option4", type="string", length=180, nullable=true)
     */
    private $option4;

    /**
     * @var string|null
     *
     * @ORM\Column(name="option5", type="string", length=180, nullable=true)
     */
    private $option5;

    /**
     * @var string|null
     *
     * @ORM\Column(name="option6", type="string", length=180, nullable=true)
     */
    private $option6;

    /**
     * @var string|null
     *
     * @ORM\Column(name="option7", type="string", length=180, nullable=true)
     */
    private $option7;

    /**
     * @var string|null
     *
     * @ORM\Column(name="option8", type="string", length=180, nullable=true)
     */
    private $option8;

    /**
     * @var string|null
     *
     * @ORM\Column(name="option9", type="string", length=180, nullable=true)
     */
    private $option9;

    /**
     * @var int
     *
     * @ORM\Column(name="weight1", type="integer", nullable=false, options= {"default": 0})
     */
    private $weight1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="weight2", type="integer", nullable=true, options= {"default": 0})
     */
    private $weight2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="weight3", type="integer", nullable=true)
     */
    private $weight3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="weight4", type="integer", nullable=true)
     */
    private $weight4;

    /**
     * @var int|null
     *
     * @ORM\Column(name="weight5", type="integer", nullable=true)
     */
    private $weight5;

    /**
     * @var int|null
     *
     * @ORM\Column(name="weight6", type="integer", nullable=true)
     */
    private $weight6;

    /**
     * @var int|null
     *
     * @ORM\Column(name="weight7", type="integer", nullable=true)
     */
    private $weight7;

    /**
     * @var int|null
     *
     * @ORM\Column(name="weight8", type="integer", nullable=true)
     */
    private $weight8;

    /**
     * @var int|null
     *
     * @ORM\Column(name="weight9", type="integer", nullable=true)
     */
    private $weight9;

    /**
     * @var bool
     *
     * @ORM\Column(name="isImage", type="boolean", nullable=false)
     */
    private $isImage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getOption1(): ?string
    {
        return $this->option1;
    }

    public function setOption1(string $option1): self
    {
        $this->option1 = $option1;

        return $this;
    }

    public function getOption2(): ?string
    {
        return $this->option2;
    }

    public function setOption2(?string $option2): self
    {
        $this->option2 = $option2;

        return $this;
    }

    public function getOption3(): ?string
    {
        return $this->option3;
    }

    public function setOption3(?string $option3): self
    {
        $this->option3 = $option3;

        return $this;
    }

    public function getOption4(): ?string
    {
        return $this->option4;
    }

    public function setOption4(?string $option4): self
    {
        $this->option4 = $option4;

        return $this;
    }

    public function getOption5(): ?string
    {
        return $this->option5;
    }

    public function setOption5(?string $option5): self
    {
        $this->option5 = $option5;

        return $this;
    }

    public function getOption6(): ?string
    {
        return $this->option6;
    }

    public function setOption6(?string $option6): self
    {
        $this->option6 = $option6;

        return $this;
    }

    public function getOption7(): ?string
    {
        return $this->option7;
    }

    public function setOption7(?string $option7): self
    {
        $this->option7 = $option7;

        return $this;
    }

    public function getOption8(): ?string
    {
        return $this->option8;
    }

    public function setOption8(?string $option8): self
    {
        $this->option8 = $option8;

        return $this;
    }

    public function getOption9(): ?string
    {
        return $this->option9;
    }

    public function setOption9(?string $option9): self
    {
        $this->option9 = $option9;

        return $this;
    }

    public function getWeight1(): ?int
    {
        return $this->weight1;
    }

    public function setWeight1(int $weight1): self
    {
        $this->weight1 = $weight1;

        return $this;
    }

    public function getWeight2(): ?int
    {
        return $this->weight2;
    }

    public function setWeight2(?int $weight2): self
    {
        $this->weight2 = $weight2;

        return $this;
    }

    public function getWeight3(): ?int
    {
        return $this->weight3;
    }

    public function setWeight3(?int $weight3): self
    {
        $this->weight3 = $weight3;

        return $this;
    }

    public function getWeight4(): ?int
    {
        return $this->weight4;
    }

    public function setWeight4(?int $weight4): self
    {
        $this->weight4 = $weight4;

        return $this;
    }

    public function getWeight5(): ?int
    {
        return $this->weight5;
    }

    public function setWeight5(?int $weight5): self
    {
        $this->weight5 = $weight5;

        return $this;
    }

    public function getWeight6(): ?int
    {
        return $this->weight6;
    }

    public function setWeight6(?int $weight6): self
    {
        $this->weight6 = $weight6;

        return $this;
    }

    public function getWeight7(): ?int
    {
        return $this->weight7;
    }

    public function setWeight7(?int $weight7): self
    {
        $this->weight7 = $weight7;

        return $this;
    }

    public function getWeight8(): ?int
    {
        return $this->weight8;
    }

    public function setWeight8(?int $weight8): self
    {
        $this->weight8 = $weight8;

        return $this;
    }

    public function getWeight9(): ?int
    {
        return $this->weight9;
    }

    public function setWeight9(?int $weight9): self
    {
        $this->weight9 = $weight9;

        return $this;
    }

    public function getIsimage(): ?bool
    {
        return $this->isImage;
    }

    public function setIsimage(bool $isImage): self
    {
        $this->isimage = $isImage;

        return $this;
    }
}
