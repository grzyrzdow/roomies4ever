<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use App\Entity\Roles;

/**
 * Users
 *
 * @ORM\Table(name="users", indexes={@ORM\Index(name="roleid", columns={"roleid"})})
 * @ORM\Entity
 * 
 * @UniqueEntity(
 * fields="email",
 * message="Adres email zajęty")
 */
class Users implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="googleId", type="string", length=255, nullable=true)
     */
    private $googleId;

    /**
     * @var string
     *
     * @ORM\Column(name="facebookId", type="string", length=255, nullable=true)
     */
    private $facebookId;

    /**
     * @var string
     *
     * @ORM\Column(name="fname", type="string", length=20, nullable=false)
     */
    private $fname;

    /**
     * @var string
     *
     * @ORM\Column(name="sname", type="string", length=50, nullable=false)
     */
    private $sname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=200, nullable=false)
     * 
     * @Assert\Length(
     *  min = 8,
     *  max = 4000,
     *  minMessage = "Hasło powinno zawierać minimalnie 8 znaków",
     *  maxMessage = "Hasło jest zbyt długie"
     * )
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=20, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=50, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="streetnum1", type="string", length=8, nullable=true)
     * @Assert\Regex(
     * pattern="/^[1-9][0-9]{0,6}[a-zA-Z]?/",
     * message="Podaj właśiwy numer domu")
     */
    private $streetnum1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="streetnum2", type="string", length=8, nullable=true)
     * @Assert\Regex(
     * pattern="/^[1-9][0-9]{0,6}[a-zA-Z]?/",
     * message="Podaj właśiwy numer domu")
     */
    private $streetnum2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phonenumber", type="string", length=9, nullable=true)
     * @Assert\Regex(
     * pattern = "/^[0-9]{9}/",
     * message="Podaj właściwy numer telefonu"
     * )
     */
    private $phonenumber;


    /**
     * @var float
     *
     * @ORM\Column(name="personality1", type="float", nullable=false, options={"default": 0})
     */
    private $personality1 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="personality2", type="float", nullable=false, options={"default": 0})
     */
    private $personality2 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="personality3", type="float", nullable=false, options={"default": 0})
     */
    private $personality3 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="personality4", type="float", nullable=false, options={"default": 0})
     */
    private $personality4 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="personality5", type="float", nullable=false, options={"default": 0})
     */
    private $personality5 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="personality6", type="float", nullable=false, options={"default": 0})
     */
    private $personality6 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="personality7", type="float", nullable=false, options={"default": 0})
     */
    private $personality7 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="personality8", type="float", nullable=false, options={"default": 0})
     */
    private $personality8 = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="personality9", type="float", nullable=false, options={"default": 0})
     */
    private $personality9 = 0;

    /**
     * @var \Roles
     *
     * @ORM\ManyToOne(targetEntity="Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleid", referencedColumnName="id")
     * })
     */
    private $roleid;

    /**
     * @var bool
     *
     * @ORM\Column(name="didTest", type="boolean", nullable=false, options= {"default": 0})
     */
    private $didTest = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isVip", type="boolean", nullable=false, options= {"default": 0})
     */
    private $isVip = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vipUntil", type="date", nullable=true)
     */
    private $vipUntil = null;


    public function getId()
    {
        return $this->id;
    }
    public function getFname()
    {
        return $this->fname;
    }
    public function getSname()
    {
        return $this->sname;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function getCountry()
    {
        return $this->country;
    }
    public function getCity()
    {
        return $this->city;
    }
    public function getStreet()
    {
        return $this->street;
    }
    public function getStreetnum1()
    {
        return $this->streetnum1;
    }
    public function getStreetnum2()
    {
        return $this->streetnum2;
    }
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }
    public function getRoleid()
    {
        return $this->roleid;
    }

    public function setFname($fname)
    {
        $this->fname = $fname;
    }
    public function setSname($sname)
    {
        $this->sname = $sname;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }
    public function setCountry($country)
    {
        $this->country = $country;
    }
    public function setCity($city)
    {
        $this->city = $city;
    }
    public function setStreet($street)
    {
        $this->street = $street;
    }
    public function setStreetnum1($streetnum1)
    {
        $this->streetnum1 = $streetnum1;
    }
    public function setStreetnum2($streetnum2)
    {
        $this->streetnum2 = $streetnum2;
    }
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;
    }
    public function setRoleid($role)
    {
        $this->roleid = $role;
    }
    public function getSalt()
    {
        return null;
    }
    public function getRoles()
    {
        if ($this->roleid->getId() == 1)
            return array('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER');
        elseif ($this->roleid->getId() == 2)
            return array('ROLE_MODERATOR', 'ROLE_USER');
        elseif ($this->roleid->getId() == 3)
            return array('ROLE_USER');
    }
    public function getUsername()
    {
        return $this->email;
    }
    public function eraseCredentials()
    {
    }

    public function getPersonality1()
    {
        return $this->personality1;
    }

    public function setPersonality1($personality1)
    {
        $this->personality1 = $personality1;

        return $this;
    }

    public function getPersonality2()
    {
        return $this->personality2;
    }

    public function setPersonality2($personality2)
    {
        $this->personality2 = $personality2;

        return $this;
    }

    public function getPersonality3()
    {
        return $this->personality3;
    }

    public function setPersonality3($personality3)
    {
        $this->personality3 = $personality3;

        return $this;
    }

    public function getPersonality4()
    {
        return $this->personality4;
    }

    public function setPersonality4($personality4)
    {
        $this->personality4 = $personality4;

        return $this;
    }

    public function getPersonality5()
    {
        return $this->personality5;
    }

    public function setPersonality5($personality5)
    {
        $this->personality5 = $personality5;

        return $this;
    }

    public function getPersonality6()
    {
        return $this->personality6;
    }

    public function setPersonality6($personality6)
    {
        $this->personality6 = $personality6;

        return $this;
    }

    public function getPersonality7()
    {
        return $this->personality7;
    }

    public function setPersonality7($personality7)
    {
        $this->personality7 = $personality7;

        return $this;
    }

    public function getPersonality8()
    {
        return $this->personality8;
    }

    public function setPersonality8($personality8)
    {
        $this->personality8 = $personality8;

        return $this;
    }

    public function getPersonality9()
    {
        return $this->personality9;
    }

    public function setPersonality9($personality9)
    {
        $this->personality9 = $personality9;

        return $this;
    }

    public function getDidTest()
    {
        return $this->didTest;
    }

    public function setDidTest($didTest)
    {
        $this->didTest = $didTest;

        return $this;
    }
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    public function setFacebookId(string $facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getGoogleId()
    {
        return $this->googleId;
    }

    public function setGoogleId(string $googleId)
    {
        $this->googleId = $googleId;

        return $this;
    }
    public function __toString()
    {
        return $this->email;
    }

    public function getIsVip()
    {
        return $this->isVip;
    }

    public function setIsVip(bool $isVip)
    {
        $this->isVip = $isVip;

        return $this;
    }

    public function getVipUntil()
    {
        return $this->vipUntil;
    }

    public function setVipUntil(\DateTime $vipUntil)
    {
        $this->vipUntil = $vipUntil;

        return $this;
    }
}
