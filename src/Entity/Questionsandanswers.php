<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Questionsandanswers
 *
 * @ORM\Table(name="questionsandanswers", indexes={@ORM\Index(name="answererid", columns={"answererid"}), @ORM\Index(name="requesterid", columns={"requesterid"})})
 * @ORM\Entity
 */
class Questionsandanswers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=200, nullable=false)
     */
    private $text;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="requesterid", referencedColumnName="id")
     * })
     */
    private $requesterid;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="answererid", referencedColumnName="id")
     * })
     */
    private $answererid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getRequesterid()
    {
        return $this->requesterid;
    }

    public function setRequesterid(?Users $requesterid): self
    {
        $this->requesterid = $requesterid;

        return $this;
    }

    public function getAnswererid()
    {
        return $this->answererid;
    }

    public function setAnswererid(?Users $answererid): self
    {
        $this->answererid = $answererid;

        return $this;
    }
}
