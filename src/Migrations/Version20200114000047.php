<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200114000047 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE729F519B');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE8D93D649');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE729F519B FOREIGN KEY (room) REFERENCES rooms (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE8D93D649 FOREIGN KEY (user) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE favourite DROP FOREIGN KEY FK_E05348BA729F519B');
        $this->addSql('ALTER TABLE favourite DROP FOREIGN KEY FK_E05348BA8D93D649');
        $this->addSql('ALTER TABLE favourite ADD CONSTRAINT FK_E05348BA729F519B FOREIGN KEY (room) REFERENCES rooms (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE favourite ADD CONSTRAINT FK_E05348BA8D93D649 FOREIGN KEY (user) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rooms DROP FOREIGN KEY FK_7CA11A96F132696E');
        $this->addSql('ALTER TABLE rooms ADD CONSTRAINT FK_7CA11A96F132696E FOREIGN KEY (userid) REFERENCES users (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE8D93D649');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE729F519B');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE8D93D649 FOREIGN KEY (user) REFERENCES users (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE729F519B FOREIGN KEY (room) REFERENCES rooms (id)');
        $this->addSql('ALTER TABLE Favourite DROP FOREIGN KEY FK_E05348BA729F519B');
        $this->addSql('ALTER TABLE Favourite DROP FOREIGN KEY FK_E05348BA8D93D649');
        $this->addSql('ALTER TABLE Favourite ADD CONSTRAINT FK_E05348BA729F519B FOREIGN KEY (room) REFERENCES rooms (id)');
        $this->addSql('ALTER TABLE Favourite ADD CONSTRAINT FK_E05348BA8D93D649 FOREIGN KEY (user) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rooms DROP FOREIGN KEY FK_7CA11A96F132696E');
        $this->addSql('ALTER TABLE rooms ADD CONSTRAINT FK_7CA11A96F132696E FOREIGN KEY (userid) REFERENCES users (id)');
    }
}
